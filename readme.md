# Extended Colormaps for OpenCV (C++)

## Motivation

[OpenCV colormaps support](https://docs.opencv.org/3.4.2/d3/d50/group__imgproc__colormap.html) is  somewhat limited:

* There are only 13 colormaps readily available.
* They can be applied to an image with [cv::applyColormap()](https://docs.opencv.org/3.4.2/d3/d50/group__imgproc__colormap.html), but there is no interface to access the colors directly for other uses.

With this library, you can use the **80** colormaps provided by  [Matplotlib](https://matplotlib.org/tutorials/colors/colormaps.html) with OpenCV in C++ to colorize images or with the drawing functions.

## Contents

* Colormap data as CV_8UC3 arrays to be used with [cv::applyColormap()](https://docs.opencv.org/3.4.2/d3/d50/group__imgproc__colormap.html)
* The colormaps are also exposed as arrays of cv::Scalar ("palettes"), to be used with OpenCV drawing functions.

## How to use

### Requierements
[OpenCV](https://opencv.org/), of course (you will need version > 3.3.0 to use [cv::applyColormap()](https://docs.opencv.org/3.4.2/d3/d50/group__imgproc__colormap.html)).

### Installation

Just download the *include/* directory and put it with your project files.

### Colorize images


### Colors for drawing functions


## List of colormaps available

* Generate with Matplotlib + images.

## Todo

* Colormap docs
* Examples
