// All colormaps from Matplotlib.
// This file was generated by extract_plt_cmaps.py
// with cmapy cmapy.__version__ and Matplotlib 2.2.2.
// See https://gitlab.com/cvejarano-oss/ecmaps

#include "colormaps/pu_sequential.h"
#include "colormaps/sequential.h"
#include "colormaps/sequential_2.h"
#include "colormaps/diverging.h"
#include "colormaps/qualitative.h"
#include "colormaps/miscellaneous.h"