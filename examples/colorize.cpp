// E-colormaps-opencv demo: colorize an image.

#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "colormaps/all.h"

int main(){

    // Read an image (color or grayscale).
    cv::Mat img_source = cv::imread("../../examples/imgs/jupiter.jpg");
    if(img_source.empty()){
        std::cout << "Error reading input image" << std::endl;
        return 1;
    }

    cv::Mat img_colorized;
    // Easy, just call cv::applyColormap(), just change the third parameter
    // to the desired colormap.
    cv::applyColorMap(img_source, img_colorized, colormaps::viridis);
    cv::imwrite("../../examples/imgs/jupiter_colorized.jpg", img_colorized);

    return 0;
}
